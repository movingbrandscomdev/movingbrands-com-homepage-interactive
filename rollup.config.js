
// import builtins from 'rollup-plugin-node-builtins'
import globals from 'rollup-plugin-node-globals'
import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'
import babel from 'rollup-plugin-babel'
import { uglify } from 'rollup-plugin-uglify'
import minify from 'rollup-plugin-babel-minify'

import pkg from './package.json'
import transformGLSL from './scripts/transformGLSL'

const name = 'mbHomepageInteractive'

export default [
  {
    input: pkg.entry,
    output: {
      file: pkg.main,
      name: name,
      sourcemap: false,
      format: 'umd'
    },
    moduleContext: { 'node_modules/whatwg-fetch/fetch.js': 'window' },
    plugins: [
      resolve(),
      transformGLSL(),
      commonjs({ include: 'node_modules/**' }),
      globals(),
      // builtins(),
      babel({
        exclude: 'node_modules/**',
        babelrc: false,
        presets: ['es2015-rollup']
      })
    ]
  },
  {
    input: pkg.entry,
    output: {
      file: pkg.min,
      name: name,
      sourcemap: false,
      format: 'umd'
    },
    plugins: [
      resolve(),
      transformGLSL(),
      commonjs({ include: 'node_modules/**' }),
      globals(),
      // builtins(),
      babel({
        exclude: 'node_modules/**',
        babelrc: false,
        presets: ['es2015-rollup']
      }),
      uglify(),
      minify()
    ]
  }
]
import commonjs from 'rollup-plugin-commonjs'
import nodeResolve from 'rollup-plugin-node-resolve'
import builtins from 'rollup-plugin-node-builtins'
import globals from 'rollup-plugin-node-globals'

import pkg from './package.json'
import transformGLSL from './scripts/transformGLSL'

const name = 'mbHomepageInteractive'

export default [
  {
    input: pkg.entry,
    output: {
      file: 'examples/mbHomepageInteractive.js',
      name: name,
      sourcemap: true,
      format: 'umd'
    },
    plugins: [
      transformGLSL(),
      nodeResolve({
        preferBuiltins: false
      }),
      commonjs(),
      globals(),
      builtins()
    ]
  }
]

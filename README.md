#movingbrands.com Homepage Interactive
##r0.6.0

This package contains the source code, tools and development setup for movingbrands.com homepage 3D/WebGL interactive. 

## Overview
This module uses [Rollup](http://rollupjs.org) to bundle the code into one single source file.

## Installation
```bash
$ git clone
$ yarn
```

## Documentation
Documentation can be found in the `/doc` folder.

Documentation for the module is automatically generated using `documentation.js`.

```bash
$ yarn doc
```
Updates documentation

## Development

```bash
$ yarn dev
```

* This calls `rollup -c`, using `rollup.config.dev.js ` for settings
* `dev` watches and launches a local server at localhost:5000
* `dev` updates the source map to make debugging easier

### three.js and Rollup
To allow Rollup to achieve better results when treeshaking, import three.js modules directly from `/src`, for example:

````
import { WebGLRenderer } from 'three/src/renderers/WebGLRenderer'
````
instead of

````
import { WebGLRenderer } from 'three'
````

## Build

```bash
$ yarn build
```
This generates one *js* source file into `/dist`.

* This calls `rollup -c`, using `rollup.config.js` for settings
* `build` transpiles the source into ES5 (browser compatibility)
* `build` creates 2 files: the regular ES5 bundle and the minified bundle

## Using the module

HTML:

```
<script src="../dist/mbHomepageInteractive.min.js"></script>
```

ES6:

```
import mbHomepageInteractive from 'mbHomepageInteractive'
```

Using any of these methods, you can use *mbHomepageInteractive* the same way:

```
var MBHI = new mbHomepageInteractive(...)
```

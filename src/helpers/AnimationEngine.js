import EventEmitter from 'eventemitter3'

import Animation from './Animation'
import { rightnow } from '../utils'

/**
 * Application-wide animation engine
 * @class AnimationEngine
 */
class AnimationEngine extends EventEmitter {
  /**
     * Initiates AnimationEngine singleton
     * @param {function} fnc optional function to be called each frame
     * @returns {AnimationEngine} animation engine
     */
  init(fnc) {
    if (!(this instanceof AnimationEngine)) {
      return new AnimationEngine(fnc)
    }
    this.running = false
    this.last = rightnow()
    this._frame = 0
    this._tick = this.tick.bind(this)
    this.animations = []
    if (fnc) {
      this.on('tick', fnc)
    }
    return this
  }

  /**
     * Starts the animation loop
     * @returns {AnimationEngine} animation engine
     */
  start() {
    if (this.running) { return }
    this.running = true
    this.last = rightnow()
    this._frame = window.requestAnimationFrame(this._tick)
    return this
  }

  /**
     * Stops the animation loop
     * @returns {AnimationEngine} animation engine
     */
  stop() {
    this.running = false
    if (this._frame !== 0) { window.cancelAnimationFrame(this._frame) }
    this._frame = 0
    return this
  }

  /**
     * Adds a new {@link Animation} to the queue
     * @param {object} target - object containing from and to values to animate between
     * @param {string} ease - type of easing (see easings.js)
     * @param {number} duration - length of animation in milliseconds
     * @param {number} delay - length of animation delay in milliseconds (optional)
     * @returns {Animation} animation object
     */
  animate(...args) {
    const animation = new Animation(...args).on('complete', id => {
      this.animations = this.animations.filter(animation => { return animation.id !== id })
    })
    this.animations.push(animation)
    return animation
  }

  /**
     * Cancels all actives animations
     */
  cancelAnimations() {
    this.animations = []
  }
  tick() {
    this._frame = window.requestAnimationFrame(this._tick)
    var time = rightnow()
    var dt = time - this.last
    this.emit('tick', dt)
    this.last = time
    this.animations.forEach(animation => animation.update(dt))
  }
}

export default new AnimationEngine()

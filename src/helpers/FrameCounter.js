import EventEmitter from 'eventemitter3'
import { rightnow } from '../utils'

/**
 * Counts frame rate
 * Derived from https://github.com/hughsk/fps
 * @class FrameCounter
 */
class FrameCounter extends EventEmitter {
  static get DATA () { return 'data' }

  constructor (opts) {
    super()

    opts = opts || {}
    this.last = rightnow()
    this.rate = 0
    this.time = 0
    this.decay = opts.decay || 1
    this.every = opts.every || 1
    this.ticks = 0
  }

  tick () {
    const time = rightnow()
    const diff = time - this.last
    const fps = diff

    this.ticks += 1
    this.last = time
    this.time += (fps - this.time) * this.decay
    this.rate = 1000 / this.time
    if (!(this.ticks % this.every)) this.emit(FrameCounter.DATA, this.rate)
    if (this.node) this.node.innerHTML = this.rate
  }
  bindElement (node) {
    this.node = node
  }
}

export default FrameCounter

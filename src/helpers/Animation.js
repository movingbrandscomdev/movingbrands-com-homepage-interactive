import EventEmitter from 'eventemitter3'
import easings from './easings'
import { randomId, mapRange, constrain } from '../utils'

/**
 * Lightweight evented animation class for tweening
 * @class Animation
 */
class Animation extends EventEmitter {
    /**
       * @param {object} target - object containing from and to values to animate between
       * @param {string} ease - type of easing (see easings.js)
       * @param {number} duration - length of animation in milliseconds
       * @param {number} delay - length of animation delay in milliseconds (optional)
       */
    constructor(target, ease, duration, delay) {
        super()
        if (!easings[ease] || easings[ease] instanceof Function === false) {
            throw new Error(`The easing ${ease} is not available, must be one of: ${Object.keys(easings).join(', ')}`, easings)
        }

        this.id = Animation.ANIMATION_ID
        this.ease = easings[ease]
        this.duration = duration
        this.progress = 0.0
        this.state = delay ? -delay : 0.0
        this.target = Object.assign({}, target)
    }

    /**
       * Updates animation and emits tick every frame (internal)
       * @param {number} dt - delta time (~16ms)
      */
    update(dt) {
        this.state += dt
        if (this.state >= 0.0) {
            this.progress = this.ease(constrain(this.state / this.duration, 0.0, 1.0))
            this.emit(Animation.TICK, mapRange(this.progress, 0.0, 1.0, this.target.from, this.target.to))
            if (this.state >= this.duration) this.emit(Animation.COMPLETE, this.id)
        }
    }

    static get COMPLETE() {
        return 'complete'
    }
    static get TICK() {
        return 'tick'
    }
    static get ANIMATION_ID() {
        return randomId(9, 'animation_')
    }
}

export default Animation
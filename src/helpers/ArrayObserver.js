import EventEmitter from 'eventemitter3'
import { findClosest } from '../utils'

/**
 * Basic helper to watch a stream of values and fetch the nearest value in a comparison array
 * @class ArrayObserver
 */
export default class ArrayObserver extends EventEmitter {
  static get CHANGE () {
    return 'change'
  }
  constructor (ref = 0, increment = Math.PI * 0.5, count = 4) {
    super()

    this.checks = []
    this.increment = increment
    this.count = count
    for (let i = 0; i <= count; i++) {
      this.checks.push(i * increment)
    }
    this.ref = ref
  }
  update (n) {
    const r = findClosest(this.checks, n)
    if (this.ref !== r) this.toggle(r)
  }
  toggle (r) {
    this.ref = r
    const n = this.checks.indexOf(this.ref)
    this.emit(ArrayObserver.CHANGE, n === this.count ? 0 : n)
  }
  validate (dir, respond) {
    respond(this.ref + this.increment * dir)
  }
}

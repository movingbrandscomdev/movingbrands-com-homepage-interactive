import MBHomepageInteractive from './MBHomepageInteractive'
import domready from 'domready'
import { DOM_REFERENCE_NODE } from './constants'
import { PRESETDATA_URL } from './presetData_constant'

const fetchPresets = url => {
  return fetch(url).then(response => {
    const contentType = response.headers.get('content-type')
    if (contentType && contentType.includes('application/json')) {
      return response.json()
    }
    throw new TypeError(['Couldn\'t load', url].join(' '))
  })
}

class MBHomepageInteractiveApp {
  init(target) {
    const debugView = {
      ui: false,
      left: 0.0,
      bottom: 0.0,
      width: 0.25,
      height: 0.25,
      dpad: true,
      vp: {
        w: 0.25,
        h: 0.25
      },
      background: 0x232323,
      camera: {
        position: {
          x: 0,
          y: 60,
          z: 140
        },
        up: {
          x: 0,
          y: 1,
          z: 0
        },
        fov: 30
      }
    }

    const options = {
      autoStart: true,
      debug: false,
      shuffle: false,
      scrollMonitor: false,
      inputMotion: true,
      ui: {
        buttons: true,
        touch: true
      }
    }

    fetchPresets(PRESETDATA_URL).then(data => {
      const mbhi = new MBHomepageInteractive(
        target,
        data,
        options,
        r => {
          console.log(r)
        }
      )
      mbhi.addView(debugView)
    }).catch(error => {
      throw new TypeError('Could not fetch preset data')
    })
  }
}

const APP = new MBHomepageInteractiveApp()

domready(() => {
  const target = document.getElementById(DOM_REFERENCE_NODE)
  if (target) {
    APP.init(target)
  } else {
    throw new TypeError(`Could not find <div> with id='${DOM_REFERENCE_NODE}'`)
  }
})

import { randomId } from './.'

const assignIds = (arr, _prepend) => arr.map(p => {
  const prepend = _prepend || 'id'
  return Object.assign(p, { id: randomId(4, [prepend, '_'].join('')) })
})

export default assignIds

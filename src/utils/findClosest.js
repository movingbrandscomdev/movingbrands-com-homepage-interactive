const findClosest = (arr, num) => arr.reduce((prev, curr) => {
  return (Math.abs(curr - num) < Math.abs(prev - num) ? curr : prev)
})

export default findClosest

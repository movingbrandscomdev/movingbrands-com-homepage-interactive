export default performance &&
performance.now ? function now () {
    return performance.now()
  } : Date.now || function now () {
    return +new Date()
  }

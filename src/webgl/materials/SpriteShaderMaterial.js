import { Color } from 'three/src/math/Color'
import { DoubleSide, FrontSide } from 'three/src/constants'
import { ShaderMaterial } from 'three/src/materials/ShaderMaterial'
import { TextureLoader } from 'three/src/loaders/TextureLoader'

import vert from '../shaders/glsl/sprite/vert.glsl'
import frag from '../shaders/glsl/sprite/frag.glsl'

import { AssetManager } from '../../presets'
import { RenderViewManager } from '../views'

/**
 * Shader material to render sprites
 */
export default class SpriteShaderMaterial extends ShaderMaterial {
  /**
   * Updates all helpers and items in the app every frame
   * @param {object} textureData - three.js Texture
   * @param {number} alpha - alpha/opacity (optional)
  */
  constructor(textureData, alpha) {
    super({
      uniforms: {
        tColorMap: {
          type: 't',
          value: textureData.texture ? textureData.texture.data : null
        },
        tAlphaMap: {
          type: 't',
          value: textureData.alphaMap ? textureData.alphaMap.data : null
        },
        useAlphaMap: {
          type: 'b',
          value: !!textureData.alphaMap
        },
        fogColor: {
          type: 'c',
          value: new Color(RenderViewManager.clearColor)
        },
        fogNear: {
          type: 'f',
          value: RenderViewManager.fog.near
        },
        fogFar: {
          type: 'f',
          value: RenderViewManager.fog.far
        },
        itemAlpha: {
          type: 'f',
          value: 0.0
        }
      },
      vertexShader: vert,
      fragmentShader: frag,
      flatShading: true,
      wireframe: false,
      side: alpha ? FrontSide : DoubleSide,
      transparent: true
    })
  }

  /**
   * Updates texture
   * @param {object} asset - Asset reference
  */
  updateColorMap(asset) {
    AssetManager.fetch(asset.id, a => this.uniforms.tColorMap.value = a.data)
    this.needsUpdate = true
  }
  /**
   * Updates alpha
   * @param {number} alpha - alpha value
  */
  updateAlpha(alpha) {
    this.uniforms.itemAlpha.value = alpha
  }
  /**
   * Updates shader every frame
  */
  update() {
    this.uniforms.fogColor.value = RenderViewManager.clearColor
  }
}
import { ShaderMaterial } from 'three/src/materials/ShaderMaterial'
import { TextureLoader } from 'three/src/loaders/TextureLoader'
import { DoubleSide, FrontSide, SmoothShading } from 'three/src/constants'
import { Color } from 'three/src/math/Color'

import vert from '../shaders/glsl/matcap/vert.glsl'
import frag from '../shaders/glsl/matcap/frag.glsl'

import { AssetManager } from '../../presets'
import { RenderViewManager } from '../views'

const defaultMatcap = '/assets/matcap/shinyblack.jpg'

/**
 * Shader material to render 3D objects in matcap style
 */
export default class MatcapShaderMaterial extends ShaderMaterial {
  /**
   * Updates all helpers and items in the app every frame
   * @param {textureData} textureData - three.js Texture
   * @param {number} alpha - alpha/opacity (optional)
   * @param {THREE.Color} baseColor - base color for object (optional)
  */
  constructor(textureData, alpha, baseColor) {
    super({
      uniforms: {
        itemAlpha: {
          type: 'f',
          value: 0.0
        },
        tMatCap: {
          type: 't',
          value: textureData.matcap ? textureData.matcap.data : null
        },
        tColorMap: {
          type: 't',
          value: textureData.texture ? textureData.texture.data : null
        },
        baseColor: {
          type: 'f',
          value: new Color(baseColor)
        },
        fogColor: {
          type: 'c',
          value: new Color(RenderViewManager.clearColor)
        },
        fogNear: {
          type: 'f',
          value: RenderViewManager.fog.near
        },
        fogFar: {
          type: 'f',
          value: RenderViewManager.fog.far
        }
      },
      vertexShader: vert,
      fragmentShader: frag,
      flatShading: true,
      wireframe: false,
      side: alpha ? FrontSide : DoubleSide,
      transparent: true
    })
    this.baseAlpha = alpha || 1.0
    
    this.extensions = {
      derivatives: false,
      fragDepth: false
    }
  }

  /**
   * Updates texturefile
   * @param {object} asset - Asset reference
  */
  updateMatcap(asset) {
    AssetManager.fetch(asset.id, a => { this.uniforms.tMatCap.value = a.data })
    this.needsUpdate = true
  }
  /**
   * Updates alpha
   * @param {number} alpha - alpha value
  */
  updateAlpha(alpha) {
    this.uniforms.itemAlpha.value = alpha * this.baseAlpha
  }
  /**
   * Updates color map
   * @param {object} asset - Asset reference
  */
  updateColorMap(asset) {
    AssetManager.fetch(asset.id, a => { this.uniforms.tColorMap.value = a.data })
    this.needsUpdate = true
  }

  /**
   * Updates shader every frame
  */
  update() {
    this.uniforms.fogColor.value = RenderViewManager.clearColor
  }
}

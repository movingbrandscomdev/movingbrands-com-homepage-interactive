import { Object3D } from 'three/src/core/Object3D'
import EventEmitter from 'eventemitter3'

import { WebGLApp } from '../.'
import { AssetManager } from '../../presets'

import motionPlugins from '../motion/motionPlugins'

import { mapRange } from '../../utils'

import {
  LayerMesh,
  LayerSprite
} from '../layers'

/**
 * Case study item based on THREE.Object3D
 * @class Item
 */
export default class Item extends Object3D {
  constructor(preset) {
    super()
    if (!preset) {
      throw new Error('Missing preset data')
    }
    this.visible = false
    this.inner = new Object3D()
    this.add(this.inner)

    this.motionPlugins = []
    this.updateCallbacks = []

    this.config = {
      id: preset.id
    }

    this.config = Object.assign(this.config, preset.data)

    this.views = Object.assign({}, preset.views)

    this.processLayers(preset.layers)
    this.add(this.inner)

    preset.plugins.forEach(plugin => {
      if (motionPlugins[plugin.name]) {
        this.addMotionPlugin(new motionPlugins[plugin.name](this.inner, plugin, this.item_id))
      } else {
        console.warn('Ignoring motion plugin with invalid name:', plugin.name)
      }
    })

  }

  addMotionPlugin(plugin) {
    this.motionPlugins.push(plugin)
  }

  removeMotionPlugin(pluginName) {
    this.motionPlugins = this.motionPlugins.filter(plugin => plugin.constructor.name !== pluginName)
  }

  processLayers(layers) {
    layers.forEach(layer => {
      if (layer.type === 'sprite') this.inner.add(new LayerSprite(layer))
      if (layer.type === '3d') this.inner.add(new LayerMesh(layer))
    })
  }
  updateSettings(type, data, u) {
    if (type === 'updateMotionPlugin') {
      this.motionPlugins.forEach((plugin, ref) => {
        const newPlugin = data.plugins[ref]
        plugin.updateProps(newPlugin.options)
        plugin.active = newPlugin.active
      })
    }
    if (type === 'updateLayer') {
      this.inner.children.forEach((child, ref) => {
        const newConfig = data.layers[ref].config
        child.updateMeshTransformation(newConfig)
      })
    }

    if (type === 'updateView') {
      this.views = data.views
      WebGLApp.emit(WebGLApp.SET_PRESET)
    }
    if (type === 'updateLayerTexture') {
      this.inner.children.forEach((child, ref) => {
        if (child.assetId === u.asset) child.updateMeshMaterial(data.layers[ref][u.type])
      })
    }
  }
  /**
   * Updates alpha value for all layers
   * @param {number} alpha - alpha value
  */
  setAlpha(alpha) {
    this.inner.children.forEach(c => c.updateAlpha(alpha))
  }
  setX(pos) {
    this.inner.position.x = pos
  }
  update(dt, ref) {
    this.motionPlugins.forEach(motionPlugin => {
      if (motionPlugin.active) {
        motionPlugin.update(dt)
      }
    })
    this.updateCallbacks.forEach(callback => callback(dt))
    this.inner.children.forEach(c => c.update())
  }
}


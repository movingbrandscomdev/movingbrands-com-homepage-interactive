import { Object3D } from 'three/src/core/Object3D'
// import { LineSegments } from 'three/src/objects/LineSegments'
// import { GridHelper } from 'three/src/helpers/GridHelper'
// import { AxesHelper } from 'three/src/helpers/AxesHelper'

const DebugHelpers = (config = { size: 100, divisions: 50, layer: 1 }) => {
  const helper = new Object3D()

  // helper.add( new GridHelper( config.size, config.divisions ) )

  // const vGrid = new GridHelper( config.size, 2 )
  // vGrid.rotation.x = Math.PI * 0.5
  // helper.add(vGrid)

  // helper.add( new AxesHelper( config.size ) )

  // helper.children.forEach(child => child.layers.set(config.layer))
  return helper
}

export default DebugHelpers

import { Vector3 } from 'three/src/math/Vector3'

const r = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
const randomHex = () => {
  return `#${Math.floor(Math.random() * 16777215).toString(16)}`
}

const rV3 = () => new Vector3(
  r(-100, 100),
  r(-100, 100),
  r(-100, -20)
)


const generateViewCameras = (rand) => {
  return {
    r: {
      position: rV3(),
      lookAt: rV3(),
      fov: rand ? r(15, 30) : 30
    },
    tl: {
      position: rV3(),
      lookAt: rV3(),
      fov: rand ? r(15, 30) : 30
    },
    bl: {
      position: rV3(),
      lookAt: rV3(),
      fov: rand ? r(15, 30) : 30
    }
  }
}

export default generateViewCameras
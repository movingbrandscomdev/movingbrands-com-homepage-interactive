/**
 * Transforms a THREE.Object3D with a configuration object containing scale, position and rotation values
 * @param {THREE.Mesh} mesh – target mesh
 * @param {object} config – transformation data
 */
const setMeshTransformation = (mesh, config) => {
  if (config.scale) {
    if (typeof config.scale === 'number') {
      mesh.scale.set(config.scale, config.scale, config.scale)
    } else {
      mesh.scale.set(
        config.scale.x || 1.0,
        config.scale.y || 1.0,
        config.scale.z || 1.0
      )
    }
  }
  if (config.rotation) {
    mesh.rotation.set(
      config.rotation.x || 0,
      config.rotation.y || 0,
      config.rotation.z || 0
    )
  }
  if (config.position) {
    mesh.position.set(
      config.position.x || 0,
      config.position.y || 0,
      config.position.z || 0
    )
  }
  if (config.hasOwnProperty('visible')) mesh.visible = config.visible
}

export default setMeshTransformation
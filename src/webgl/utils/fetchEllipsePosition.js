const fetchEllipsePosition = (fac, total, radius, sum) => {
  const theta = Math.PI * 2 / total
  const angle = Math.PI * 0.5 - theta * fac + sum

  return {
    x: radius.x * Math.cos(angle),
    y: 0,
    z: -radius.z + radius.z * Math.sin(angle)
  }
}

export default fetchEllipsePosition
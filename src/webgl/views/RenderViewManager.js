import { Color } from 'three/src/math/Color'
import { RenderView } from './.'

/**
 * Creates and manages views to be applied to main renderer in {@link WebGLApp}
 * @class RenderViewManager
 */
class RenderViewManager {
  constructor() {
    this.views = []
  }
  /**
   * Initiates {@link RenderViewManager}
   * @param {array} defaultViews - set of initial camera views
   * @param {number} width - width of screen (px)
   * @param {number} height - height of screen (px)
   * @param {boolean} debug - start in debug mode
   */
  init(defaultViews, width, height, debug) {
    this.debug = debug
    this.clearColor = new Color(0x000000)
    this.targetClearColor = new Color(0x000000)

    this.fog = {
      near: -100.0,
      far: -200.0
    }
    this.skew = {
      x: 0.0,
      y: 0.0
    }
    this.changeSpeed = 0.05

    defaultViews.forEach(view => this.views.push(new RenderView(view, width, height, this.debug)))
  }
  update(renderer, scene, width, height) {
    this.clearColor.lerp(this.targetClearColor, this.changeSpeed)
    this.views.forEach(view => {
      if (view.params.dpad && this.debug || !view.params.dpad) {
        view.updateView(scene)
        view.render(renderer, width, height)
        renderer.render(scene, view.camera)
      }
    })
  }
  resizeViews(width, height) {
    this.views.forEach(view => view.update(width, height))
  }
  addView(view, width, height) {
    this.views.push(new RenderView(view, width, height, this.debug))
  }
  removeView(view_id) {
    this.views = this.views.filter(view => view.id !== view_id)
  }
  setSkew(skew) {
    // console.log(skew)
    this.skew.x = skew.x
    this.skew.y = skew.y
  }
  updateViewCameras(update) {
    if (update.color) this.targetClearColor = new Color(update.color)
    this.views.forEach(view => {
      if (update[view.params.ui]) {
        view.setCameraTarget(update[view.params.ui], 0.05)
      }
    })
  }
  debugMode(setDebug) {
    this.debug = !!setDebug ? setDebug : !this.debug
    this.views.filter(view => view.params.dpad).forEach(view => {
      this.debug ? view.dpad.on() : view.dpad.off()
    })
  }
}

export default new RenderViewManager()
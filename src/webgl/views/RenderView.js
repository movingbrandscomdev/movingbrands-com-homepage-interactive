import { PerspectiveCamera } from 'three/src/cameras/PerspectiveCamera'
import { Vector3 } from 'three/src/math/Vector3'
import { Color } from 'three/src/math/Color'

import {
  lerp,
  randomId
} from '../../utils'
import { Controller } from '../../ui'
import DebugControlPad from '../controls/DebugControlPad'
import { RenderViewManager } from './.'

/**
 * Camera view
 */
class RenderView {
  /**
   * Creates a new view
   * @param {object} data - view settings
   * @param {number} width - width
   * @param {number} height - height
   * @param {boolean} debug - debug mode
   */
  constructor(data, width, height, debug) {
    this.id = RenderView.RENDERVIEW_ID
    this.params = Object.assign({
      clearColor: new Color(data.background),
      dpadSize: 0.25,
      changeSpeed: 0.001,
      base: new Vector3(0,0,100)
    }, data)
    this.init(width, height, debug)

  }

  /**
   * Initiates view
   * @param {number} width - width
   * @param {number} height - height
   * @param {boolean} debug - debug mode
   */
  init(width, height, debug) {
    this.motion = {
      deform: 1.0
    }
    this.camera = new PerspectiveCamera(this.params.camera.fov, width / height, 1, 10000)

    this.lookAtStore = new Vector3(0, 0, 0)

    this.camera.position.set(
      this.params.camera.position.x,
      this.params.camera.position.y,
      this.params.camera.position.z
    )
    this.camera.up.set(0, 1, 0)
    this.updateCamera()
    this.update(width, height)

    if (this.params.dpad) {
      this.addControls(width, height, debug)
      this.camera.layers.enable(2)
      this.camera.layers.enable(1)
    }
  }

  /**
   * Updates the three.js camera (every frame)
   */
  updateCamera() {
    this.camera.position.lerp(this.params.base, 1.0 - this.motion.deform)
    this.camera.fov = lerp(30, this.params.camera.fov, this.motion.deform)
    this.camera.updateProjectionMatrix()
  }

  /**
   * Update view camera (every frame)
   * @param {object} cameraData - camera update object
   * @param {number} transition - speed of transition (0.0-1.0)
   */
  setCameraTarget(cameraData, transition) {
    this.params.changeSpeed = transition
    this.target = {
      position: cameraData.position,
      fov: cameraData.fov,
      lookAt: cameraData.lookAt,
      clearColor: new Color(cameraData.color)
    }
  }

  /**
   * Renders camera view to master renderer
   * @param {object} renderer - three.js master renderer
   */
  render(renderer) {
    renderer.setViewport(this.settings.viewport.fx, this.settings.viewport.fy, this.settings.viewport.tx, this.settings.viewport.ty)
    renderer.setScissor(this.settings.left, this.settings.bottom, this.settings.width, this.settings.height)
    renderer.setScissorTest(true)
    renderer.setClearColor(this.params.dpad ? this.params.clearColor : RenderViewManager.clearColor)
  }

  /**
   * Updates camera target
   * @param {object} scene - three.js master scene
   */
  updateView(scene) {
    if (this.target) {
      this.camera.position.lerp(this.target.position, this.params.changeSpeed)
      this.camera.fov = lerp(this.camera.fov, this.target.fov, this.params.changeSpeed)
      this.lookAtStore.lerp(this.target.lookAt, this.params.changeSpeed)
      this.camera.updateProjectionMatrix()
    }
    this.camera.lookAt(this.lookAtStore)
  }

  /**
   * Adds controls (and optional debug mode) to view
   * @param {number} width - width of screen (px)
   * @param {number} height - height of screen (px)
   * @param {boolean} debug - debug mode
   */
  addControls(width, height, debug) {
    this.dpad = new DebugControlPad(
      debug,
      this.params.width * width,
      this.params.height * height,
      this.camera)
  }

  /**
   * Updates camera view size (triggered on window resize)
   * @param {number} width - width of camera view (px)
   * @param {number} height - height of camera view (px)
   */
  update(width, height) {
    this.settings = {
      left: Math.floor(width * this.params.left),
      bottom: Math.floor(height * this.params.bottom),
      width: Math.floor(width * this.params.width),
      height: Math.floor(height * this.params.height)
    }

    this.settings.viewport = {
      fx: this.params.dpad ? this.settings.left : 0.0,
      fy: this.params.dpad ? this.settings.bottom : 0.0,
      tx: width * this.params.vp.w,
      ty: height * this.params.vp.h
    }

    if (this.dpad) this.dpad.update(this.params.width * width, this.params.height * height)

    this.camera.aspect = this.settings.width / this.settings.height
    this.camera.updateProjectionMatrix()
  }
  static get RENDERVIEW_ID() { return randomId(4, 'renderview_') }
}

export default RenderView
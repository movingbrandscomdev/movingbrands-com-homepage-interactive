/**
 * Base class for plugins that handle {@link Item} motion
 * @class MotionPlugin
 */
export default class MotionPlugin {
  constructor(targetMesh, plugin) {
    if (!targetMesh) return
    this.mesh = targetMesh
    this.id = plugin.id
    this._active = plugin.active || false
    this.opts = Object.assign({}, plugin.options) || {}
  }
  set active(active) {
    this._active = active
    if (!this._active) this.reset()
  }
  reset() {
    this.mesh.children.forEach(child => {
      child.children.forEach(subchild => {
        subchild.position.set(0, 0, 0)
        subchild.rotation.set(0, 0, 0)
      })
    })
  }
  get active() {
    return this._active
  }
  updateProps(opts, active) {
    this.opts = Object.assign({}, opts)
  }
  static create(targetMesh, plugin) {
    return new MotionPlugin(targetMesh, plugin)
  }
}
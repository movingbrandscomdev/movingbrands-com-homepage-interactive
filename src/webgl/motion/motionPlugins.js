import Drift from './plugins/Drift'
import Randomise from './plugins/Randomise'
import Revolve from './plugins/Revolve'

export {
  Drift,
  Randomise,
  Revolve
}
export default {
  Drift,
  Randomise,
  Revolve
}
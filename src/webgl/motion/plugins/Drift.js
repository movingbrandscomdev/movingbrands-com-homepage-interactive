import MotionPlugin from '../MotionPlugin'

export default class Drift extends MotionPlugin {
  constructor(targetMesh, plugin) {
    super(targetMesh, plugin)
    this.tot = 0
  }
  update(dt) {
    this.tot += dt / 1000
    this.mesh.children.forEach((child, i) => {
      const fac = this.mesh.children.length - i
      child.children.forEach(subchild => {
        subchild.position.x = Math.sin(fac * this.tot * this.opts.x)
        subchild.position.y = Math.sin(fac * this.tot * this.opts.y)
        subchild.position.z = Math.sin(fac * this.tot * this.opts.z)
      })
    })
  }
}

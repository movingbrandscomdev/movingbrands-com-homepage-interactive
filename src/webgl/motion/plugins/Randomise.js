import MotionPlugin from '../MotionPlugin'

export default class Randomise extends MotionPlugin {
  constructor(targetMesh, opts) {
    super(targetMesh, opts || {})
  }
  update(dt) {
    this.mesh.children.forEach((child, i) => {
      child.position.x += Math.sin(i + dt / 1000)
      child.position.y += Math.sin(i + dt / 1000)
      child.position.z += Math.sin(i + dt / 1000)
    })
  }
}

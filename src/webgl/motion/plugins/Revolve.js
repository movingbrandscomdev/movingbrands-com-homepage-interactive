import MotionPlugin from '../MotionPlugin'

export default class Revolve extends MotionPlugin {
  constructor(targetMesh, plugin) {
    super(targetMesh, plugin)
  }
  update(dt) {
    this.mesh.children.forEach((child, i) => {
      const fac = this.mesh.children.length - i
      child.children.forEach(subchild => {
        subchild.rotation.x -= fac * this.opts.x
        subchild.rotation.y -= fac * this.opts.y
        subchild.rotation.z -= fac * this.opts.z
      })
    })
  }
}
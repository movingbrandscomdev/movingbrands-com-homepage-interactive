import throttle from 'lodash.throttle'

import { WebGLApp } from '../.'
import { TouchHandler } from '../../ui'
import {
  constrain,
  lerp,
  mapRange,
  isMobile,
  diff,
  hasDeviceOrientation
} from '../../utils'
import { AnimationEngine } from '../../helpers'
import { Controller } from '../../ui'
import { RenderViewManager } from '../views'

import { generateViewCameras } from '../utils'
import { html } from '../../dom'

/**
 * The main controller that organises and delegates motion and position throughout the app
 * @class MotionPath
 */
export default class MotionPath {
  constructor() {
    this.settings = {
      mobile: isMobile()
    }
    this.motion = {
      duration: 400,
      active: false,
      queue: null,
      increment: 20
    }

    this.updateCallbacks = []

    this.handleDeviceOrientation = this.handleDeviceOrientation.bind(this)

    WebGLApp.on('app-ready', () => {
      if (this.settings.mobile) {
        this.initMobilePath()
      } else {
        this.initDesktopPath()
      }
    })

    this.state = {
      active: 0
    }

    this.radius = {
      x: 90,
      z: 140
    }

    this.resize()
    this.set = this.set.bind(this)
  }
  initDesktopPath() {
    window.addEventListener('mousemove', throttle(this.handleMouse.bind(this), 16), false)

  }
  initMobilePath() {
    if (hasDeviceOrientation()) {
      window.addEventListener('deviceorientation', throttle(this.handleDeviceOrientation, 32), true)
    }

    WebGLApp.items.forEach((item, num) => item.position.x = num * this.motion.increment)

    this.touch = new TouchHandler(WebGLApp.params.target, WebGLApp.ITEM_COUNT + 1)
    this.touch.on('change', () => this.set(this.touch.activePanel))

    this.updateCallbacks.push(this.updateMobile.bind(this))
  }
  updateMobile(dt) {
    this.touch.update(dt)
    WebGLApp.scene.position.x = -this.touch.x * this.motion.increment
  }
  set(p) {
    const d = p - this.state.active
    if (d !== 0) {
      this.moveTo(d)
    }
  }
  moveTo(direction) {
    this.movePath(this.state.active + direction, constrain(direction, -1, 1))
  }
  handleDeviceOrientation(e) {
    const y = mapRange(e.gamma || 0, -60, 60, -0.25, 0.25)
    const x = mapRange(e.beta || 0, -60, 60, -0.25, 0.25)

    WebGLApp.targetRotation.x = x
    WebGLApp.targetRotation.y = y
  }
  handleMouse(e) {
    const move = {
      x: -1.0 + e.pageX / window.innerWidth * 2,
      y: -1.0 + e.pageY / window.innerHeight * 2
    }
    WebGLApp.targetRotation.y = constrain(move.x / 4, -0.3, 0.3)
    WebGLApp.targetRotation.x = constrain(move.y / 4, -0.3, 0.3)
    RenderViewManager.setSkew({ x: e[0] / window.innerWidth, y: e[1] / window.innerHeight })
  }
  movePath(d, direction) {
    if (!this.motion.active) {
      if (d > WebGLApp.ITEM_COUNT) d = 0
      if (d < 0) d = WebGLApp.ITEM_COUNT

      const movement = {
        out: this.state.active,
        in: d
      }
      this.state.active = movement.in
      this.motion.active = true
      const o = WebGLApp.items[movement.out]
      const i = WebGLApp.items[d]

      Controller.update(d)
      AnimationEngine.animate({ id: 'moveOut', from: 1.00, to: 0.0 }, 'easeInOutSin', this.motion.duration)
        .on('tick', v => {
          o.setX((1.0 - v) * -direction * 2)
          o.setAlpha(v)
        })
        .on('complete', () => {
          WebGLApp.emit(WebGLApp.ITEM_CHANGED, i.config.id)
          RenderViewManager.updateViewCameras(i.views)

          o.visible = false
          i.visible = true
          AnimationEngine.animate({ id: 'moveIn', from: 0.0, to: 1.00 }, 'easeInOutSin', this.motion.duration)
            .on('tick', v => {
              i.setX((1.0 - v) * direction * 2)
              i.setAlpha(v)
            })
            .on('complete', () => {
              this.motion.active = false
              this.resolveQueue()
            })
        })
    } else {
      this.motion.queue = {
        d: d,
        direction: direction
      }
    }
  }

  resolveQueue() {
    if (this.motion.queue) {
      this.movePath(this.motion.queue.d, this.motion.queue.direction)
      this.motion.queue = null
    }
  }
  update(dt) {
    this.updateCallbacks.forEach(callback => callback(dt))

  }
  // setupLogoTrigger(el) {
  //     if (el) {
  //         el.style.pointerEvents = 'initial'
  //         el.addEventListener('click', () => {
  //             const v = generateViewCameras(true)
  //             console.log(v)
  //             RenderViewManager.updateViewCameras(v)
  //         })
  //     }
  // }
  resize() {
    const { innerWidth } = window
    this.settings.mobile = isMobile()

    if (innerWidth < 500) {
      this.motion.duration = 225
    }
    if (innerWidth > 500 && innerWidth < 1000) {
      this.motion.duration = 340
    }
    if (innerWidth > 1000) {
      this.motion.duration = 400
    }
  }
}


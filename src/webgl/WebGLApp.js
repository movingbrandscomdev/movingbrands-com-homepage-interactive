import { WebGLRenderer } from 'three/src/renderers/WebGLRenderer'
import { Scene } from 'three/src/scenes/Scene'
import { Euler } from 'three/src/math/Euler'

import EventEmitter from 'eventemitter3'
import throttle from 'lodash.throttle'

import { namespace } from '../dom/css'
import {
  MotionPath
} from './motion'
import {
  FrameCounter,
  AnimationEngine
} from '../helpers'
import {
  chooseDevicePixelRatio,
  lerp
} from '../utils'
import { RenderViewManager } from './views'
import { DebugHelpers } from './utils'

/**
 * Controls the three WebGLRenderer, Scene and animation loop (singleton)
 */
class WebGLApp extends EventEmitter {
  static get DEBUG_VISIBILITY() {
    return 'show-debug'
  }
  static get RESIZE() {
    return 'resize'
  }
  static get APP_READY() {
    return 'app-ready'
  }
  /**
       * Initiates WebGLApp.
       * @param {HTMLElement} target - a valid HTML div that can act as a container for the WebGLApp canvas.
       * @param {object} config - object with settings
       * @param {Array} views - views for {@link RenderViewManager}
       * @returns {WebGLApp} – WebGLApp singleton
       */
  init(target, config, views) {
    this.params = {
      target: target || document.body,
      width: target.offsetWidth,
      height: target.offsetHeight,
      displayDebug: config.debug ? config.debug : false,
      inputMotion: config.inputMotion
    }

    // array of camera views
    this.views = []

    // main array of items in the carousel (this.scene contains entire scenegraph)
    this.items = []

    // helper objects and any other items that need an update loop
    this.helpers = []

    // 'update' methods that are called every frame, with optional 'dt' parameter
    this.updateCallbacks = []
    // 'resize' methods that are called when the window is resized
    this.resizeCallbacks = []

    // create instance of threejs renderer
    this.createRenderer(target)

    // create camera views
    RenderViewManager.init(views, this.params.width, this.params.height, this.params.displayDebug)

    this.createScene()
    this.createDebugMode()
    if (this.params.displayDebug) {
      // create optional event-based frame counter
      this.fps = new FrameCounter({ every: 60 })
      this.updateCallbacks.push(this.fps.tick.bind(this.fps))
    } else {
      // standard item update
    }
    this.updateCallbacks.push(this.updateItems.bind(this))

    // add event handlers with throttle (millisecond is throttle delay)
    if (this.params.inputMotion) this.addRendererEventHandlers(16)

    // trigger first render outside of loop
    this.render()

    return this
  }

  /**
       * Creates three.js WebGLRenderer and adds to @param target
       * @param {HTMLElement} target - target DOM element
       */
  createRenderer(target) {
    // create instance of three.js renderer and add to dom
    this.renderer = new WebGLRenderer({
      antialias: true,
      failIfMajorPerformanceCaveat: true
    })

    // optional: hi-dpi canvas (incurs significant performance cost)
    this.renderer.setPixelRatio(chooseDevicePixelRatio())
    this.renderer.setSize(this.params.width, this.params.height)
    target.appendChild(this.renderer.domElement)
    this.renderer.domElement.className = namespace
  }

  /**
       * (internal) adds throttled event listeners to window
       * @param {number} inc - increment in milliseconds for throttle
       */
  addRendererEventHandlers(inc) {
    // window resize listener
    window.addEventListener('resize', throttle(this.resize.bind(this), inc), true)

    this.targetRotation = new Euler()
    this.updateCallbacks.push(this.lerpRotation.bind(this))
  }

  /**
       * (optional) adds debug helpers to scene
       */
  createDebugMode() {
    this.scene.add(DebugHelpers({
      size: 100,
      divisions: 10,
      layer: 1
    }))

    // add key commands
    window.addEventListener('keydown', throttle(this.handleKeyDown.bind(this), 750))
  }
  handleKeyDown(e) {
    if (e.keyCode === 85 && e.metaKey) {
      RenderViewManager.debugMode()
      this.emit(WebGLApp.DEBUG_VISIBILITY, RenderViewManager.debug)
    } else if (e.keyCode === 37) {
      // left
      this.path.moveTo(-1)
    } else if (e.keyCode === 39) {
      // right
      this.path.moveTo(1)
    }
  }

  /**
       * Reorders the case studies if in editor mode
       * @param {array} sequence - sequence of item ids
       */
  setOrder(sequence = this.items.map(i => i.config.id)) {
    this.order = {}
    sequence.forEach((id, i) => { this.order[id] = i })
  }
  createScene() {
    this.scene = new Scene()

    // create a new motion path which guides the movement of items in the scene
    this.path = new MotionPath(this.params.target)
    this.path.resize()
    this.resizeCallbacks.push(this.path.resize.bind(this.path))
    this.helpers.push(this.path)
    // start the app
    AnimationEngine.init(this.render.bind(this))
  }
  resize(target) {
    this.emit(WebGLApp.RESIZE)
    this.resizeCallbacks.forEach(callback => callback())
    this.renderer.setPixelRatio(chooseDevicePixelRatio())
    this.params.width = this.params.target.offsetWidth
    this.params.height = this.params.target.offsetHeight
    RenderViewManager.resizeViews(this.params.width, this.params.height)
    this.renderer.setSize(this.params.width, this.params.height)
  }
  lerpRotation() {
    this.scene.rotation.set(
      lerp(this.scene.rotation.x, this.targetRotation.x, 0.05),
      lerp(this.scene.rotation.y, this.targetRotation.y, 0.05),
      0)
  }
  updateItem(u, type, data) {
    this.items.find(i => i.config.id === u.preset).updateSettings(type, data, u)
  }

  start() {
    AnimationEngine.start()
  }

  stop() {
    AnimationEngine.stop()
  }

  render(dt) {
    this.updateCallbacks.forEach(callback => callback(dt))
    this.helpers.forEach(helper => helper.update(dt))
    RenderViewManager.update(this.renderer, this.scene, this.params.width, this.params.height)
  }
  updateItems(dt) {
    this.items.forEach((item, ref) => {
      item.update(dt)
    })
  }

  /**
       * Reorders the case studies if in editor mode
       * @param {Item} item - new {@link Item} to add to scene
       */
  addItem(item) {
    this.items.push(item)
    this.scene.add(item)
  }
  get ITEMS() {
    return this.items
  }
  get ITEM_COUNT() {
    return this.items.length - 1
  }
  get SET_PRESET() {
    return 'set-preset'
  }
  get ITEM_CHANGED() {
    return 'item-changed'
  }
  get DEBUG() {
    return this.params.displayDebug
  }
}

export default new WebGLApp()

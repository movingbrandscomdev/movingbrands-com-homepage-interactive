import DiamondGrid from './DiamondGrid'
import RandomGrid from './RandomGrid'

export {
  DiamondGrid,
  RandomGrid
}

export default {
  DiamondGrid,
  RandomGrid
}
import { Vector2 } from 'three/src/math/Vector2'
import { Vector3 } from 'three/src/math/Vector3'
import { randomFloat } from '../../utils'

class RandomGrid {
  static get DEFAULT_PARAMS() {
    return {
      x: 5,
      y: 5,
      z: 0,
      count: 20
    }
  }
  constructor(
    params = RandomGrid.DEFAULT_PARAMS) {

    this.vertices = []
    this.rotations = []

    this.params = {
      x: params.x,
      y: params.y,
      z: params.z,
      rotation: params.rotation,
      count: params.count
    }

    this.init(this.params.x, this.params.y, this.params.z, this.params.count, params.rotation)
  }
  init(x, y, z, count, rotation) {
    // offset for centre point of grid
    // create grid of points
    for (let i = 0; i < count; i++) {
      this.vertices.push(new Vector3(
        randomFloat(-x, x),
        randomFloat(-y, y),
        randomFloat(-z, z)
      ))
      this.rotations.push(new Vector3(
        randomFloat(-rotation, rotation),
        randomFloat(-rotation, rotation),
        randomFloat(-rotation, rotation)
      ))
    }
  }
  get count() {
    return this.params.count
  }
}


export default RandomGrid

import { Vector2 } from 'three/src/math/Vector2'
import { Vector3 } from 'three/src/math/Vector3'

class DiamondGrid {
    static get DEFAULT_PARAMS() {
        return {
            rows: 5,
            columns: 5,
            unit: {
                x: 10,
                y: 10
            }
        }
    }
    constructor(
        params = DiamondGrid.DEFAULT_PARAMS) {

        this.vertices = []
        this.params = {
            xr: params.columns,
            yr: params.rows,
            xu: params.unit.x,
            yu: params.unit.y
        }

        this.init(this.params.xr, this.params.yr, this.params.xu, this.params.yu)
    }
    init(xr, yr, xu, yu, shuffle) {
        // offset for centre point of grid
        const o = new Vector2(((xr - 1) * xu) * 0.5, ((yr - 1) * yu) * 0.5)
        const temp = []
        // create grid of points
        for (let x = 0; x < xr; x++) {
            for (let y = 0; y < yr; y++) {
                let vx = (x * xu) - o.x
                isOdd(y) ? vx+= xu / 2.0 : null
                this.vertices.push(new Vector3(vx, (y * yu) - o.y, 0))
            }
        }
    }
    get count() {
        return this.params.xr * this.params.yr
    }
}

const isOdd = n => Math.abs(n % 2) == 1

export default DiamondGrid

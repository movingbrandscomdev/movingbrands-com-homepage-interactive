import { Geometry } from 'three/src/core/Geometry'
import { Mesh } from 'three/src/objects/Mesh'
import { Object3D } from 'three/src/core/Object3D'
import { AmbientLight } from 'three/src/lights/AmbientLight'

import { AssetManager } from '../../presets'
import { MatcapShaderMaterial } from '../materials'
import { setMeshTransformation } from '../utils'
import generators from '../generators'
import { RenderViewManager } from '../views'

/**
 * three.js object containing a 3D object
 */
export default class LayerMesh extends Object3D {
  /**
   * Creates a new {@link LayerMesh}.
   * @param {object} layer - object containing layer data
   */
  constructor(layer) {
    super()
    this.config = layer.config || { opacity: 1.0 }
    this.init(layer)
    this.layer_id = layer.id
  }

  /**
   * Initiates {@link LayerMesh}.
   * @param {object} presetLayer - object containing layer data
   */
  init(presetLayer) {
    AssetManager.fetch(presetLayer.assets, layerAssets => {
      const { gltf, matcap } = layerAssets
      this.processData(gltf.data.scene, layerAssets, presetLayer.config)
      this.updateMeshTransformation(presetLayer.config)
    })
  }
  processData(src, layerAssets, config) {

    if (layerAssets.matcap) {
      this.itemMaterial = new MatcapShaderMaterial(layerAssets, this.config.opacity)
    } else {
      this.lighting = new AmbientLight(0xFFFFFF, 0.0)
      this.add(this.lighting)
    }
    src.traverse(child => {
      if (child instanceof Mesh) {
        const { smooth, instancing, center } = config

        if (smooth) child.geometry.computeVertexNormals()
        if (center) child.geometry.center()

        const mesh = new Mesh(
          new Geometry().fromBufferGeometry(child.geometry),
          this.itemMaterial || child.material.clone()
        )
        

        if (smooth) {
          mesh.geometry.mergeVertices()
          mesh.geometry.computeVertexNormals()
        }

        if (instancing) {
          if (generators[instancing.type]) {
            const g = new generators[instancing.type](instancing.params)
            g.vertices.forEach((v, i) => {
              const m = mesh.clone()
              m.position.set(v.x, 0, v.y)
              if (g.rotations) {
                const r = g.rotations[i]
                m.rotation.set(r.x, r.y, r.z)
              }
              this.add(m)
            })
          } else {
            console.warn(`${instancing.type} is not a valid generator`)
          }
        } else {
          this.add(mesh)
        }
      }
    })

  }

  /**
   * Updates the transformation (scale, rotation, position) of the object
   * @param {object} config - settings object containing transformation to set with {@link setMeshTransformation}
   */
  updateMeshTransformation(config) {
    setMeshTransformation(this, config)
  }
  /**
   * Updates the item alpha/opacity
   * @param {number} alpha - alpha value
   */
  updateAlpha(alpha) {
    if (this.itemMaterial) {
      this.itemMaterial.updateAlpha(alpha)
    } else if (this.lighting) {
      
      this.lighting.intensity = alpha * 0.95
    }
  }
  updateMeshMaterial(asset) {
    if (this.itemMaterial) this.itemMaterial.updateMatcap(asset)
  }
  /**
   * Update the matcap shader material (internal, every frame)
   */
  update() {
    if (this.itemMaterial) this.itemMaterial.update()
  }
}

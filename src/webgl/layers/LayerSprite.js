import { PlaneBufferGeometry } from 'three/src/geometries/PlaneGeometry'
import { Mesh } from 'three/src/objects/Mesh'
import { Object3D } from 'three/src/core/Object3D'

import { AssetManager } from '../../presets'
import { SpriteShaderMaterial } from '../materials'
import { setMeshTransformation } from '../utils'

/**
 * three.js object containing a 2D image sprite
 */
export default class LayerSprite extends Object3D {
  /**
   * Creates a new {@link LayerSprite}.
   * @param {object} layer - object containing layer data
   */
  constructor(layer) {
    super()
    this.config = layer.config || { opacity: 0.0 }
    this.layer_id = layer.id
    this.init(layer)
  }
  /**
   * Initiates {@link LayerSprite}.
   * @param {object} presetLayer - object containing layer data
   */
  init(presetLayer) {
    AssetManager.fetch(presetLayer.assets, result => {
      this.itemMaterial = new SpriteShaderMaterial(result, 0.0)
      const mesh = new Mesh(
        new PlaneBufferGeometry(25, 25),
        this.itemMaterial
      )
      this.add(mesh)
      this.updateMeshTransformation(presetLayer.config)
    })
  }

  /**
   * Updates the transformation (scale, rotation, position) of the sprite
   * @param {object} config - settings object containing transformation to set with {@link setMeshTransformation}
   */
  updateMeshTransformation(config) {
    setMeshTransformation(this, config)
  }

  /**
   * Updates the sprite texture
   * @param {object} asset - image asset
   */
  updateMeshMaterial(asset) {
    this.itemMaterial.updateColorMap(asset)
  }
  /**
   * Updates the item alpha/opacity
   * @param {number} alpha - alpha value
   */
  updateAlpha(alpha) {
    this.itemMaterial.updateAlpha(alpha)
  }

  /**
   * Update the sprite shader material (internal, every frame)
   */
  update() {
    this.itemMaterial.update()
  }
}

import OrbitControls from './OrbitControls'
import { html } from '../../dom'

/**
 * @module DebugControlPad
 * Creates a small inset element which can be used to control debugging camera
 */

export default class DebugControlPad {
  constructor(debug, width, height, camera) {
    this.init(debug, width, height, camera)
  }
  init(debug, width, height, camera) {
    this.dpad = html('div', debug ? 'd-pad show' : 'd-pad')
    this.update(width, height)
    document.body.appendChild(this.dpad)

    this.controls = new OrbitControls(camera, this.dpad)
    this.controls.damping = 0.2
    this.controls.enabled = debug
  }
  update(width, height) {
    this.dpad.style.width = `${width}px`
    this.dpad.style.height = `${height}px`
  }
  off() {
    this.controls.enabled = false
    this.dpad.classList.remove('show')
  }
  on() {
    this.controls.enabled = true
    this.dpad.classList.add('show')
  }
}

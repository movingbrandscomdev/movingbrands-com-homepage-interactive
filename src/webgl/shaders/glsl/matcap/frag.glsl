uniform sampler2D tMatCap;
uniform float itemAlpha;
uniform vec3 fogColor;
uniform float fogNear;
uniform float fogFar;

varying float zIndex;
varying vec2 vN;

void main() {
    vec3 base = texture2D( tMatCap, vN ).rgb;
    float alph = smoothstep(fogNear, fogFar, zIndex);
    vec3 depthColor = mix(base.rgb, fogColor, alph);
    gl_FragColor.rgb = mix(fogColor, depthColor, itemAlpha);
    gl_FragColor.a = itemAlpha;
}

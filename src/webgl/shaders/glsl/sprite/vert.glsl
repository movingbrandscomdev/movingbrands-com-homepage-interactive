varying vec2 vUv;
varying float zIndex;
void main() {
    vUv = uv;
    vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
    zIndex = mvPosition.z;
    gl_Position = projectionMatrix * mvPosition;
}

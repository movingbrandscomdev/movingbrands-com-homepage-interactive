// basic sprite rendering shader with WIP anti-aliasing
#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D tColorMap;
uniform sampler2D tAlphaMap;
uniform bool useAlphaMap;
uniform vec3 fogColor;
uniform float itemAlpha;
uniform float fogNear;
uniform float fogFar;

varying vec2 vUv;
varying float zIndex;

void main() {
    vec4 base = texture2D( tColorMap, vUv );
    float alph = smoothstep(fogNear, fogFar, zIndex);
    vec3 depthColor = mix(base.rgb, fogColor, alph);
    gl_FragColor.rgb = mix(fogColor, depthColor, itemAlpha);
    if (useAlphaMap) {
        vec3 alphaTex = texture2D(tAlphaMap, vUv).rgb;
        gl_FragColor.a = 1.0 - (alphaTex.r + alphaTex.g + alphaTex.b)/3.0;
    } else {
        gl_FragColor.a = base.a;
    }
}
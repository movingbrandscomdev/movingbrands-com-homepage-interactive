import { Texture } from 'three/src/textures/Texture'
import {
  LinearEncoding,
  RGBFormat,
  RGBAFormat,
  ClampToEdgeWrapping,
  LinearMipMapLinearFilter,
  LinearFilter
} from 'three/src/constants'
import loadImage from '../../utils/loadImage'
import WebGLApp from '../WebGLApp'
const noop = () => { }

const loadTexture = (path, opt, cb) => {
  if (typeof opt === 'function') {
    cb = opt
    opt = {}
  }
  opt = Object.assign({}, opt)
  cb = cb || noop

  const texture = new Texture()
  texture.name = path
  texture.encoding = opt.encoding || LinearEncoding
  setTextureParams(path, texture, opt)
  loadImage(path, {
    crossOrigin: 'Anonymous'
  }, (err, image) => {
    if (err) {
      const msg = `Could not load texture ${path}`
      console.error(msg)
      return cb(new Error(msg))
    }
    texture.image = image
    texture.needsUpdate = true
    if (WebGLApp.renderer) {
      // Force texture to be uploaded to GPU immediately,
      // this will avoid "jank" on first rendered frame
      WebGLApp.renderer.setTexture2D(texture, 0)
    }
    cb(null, texture)
  })
  return texture
}

export default class TexLoader {
  load(path, response) {
    loadTexture(path, {}, (err, tex) => response(tex))
  }
}

const setTextureParams = (url, texture, opt) => {
  if (typeof opt.flipY === 'boolean') texture.flipY = opt.flipY
  if (typeof opt.mapping !== 'undefined') {
    texture.mapping = opt.mapping
  }
  if (typeof opt.format !== 'undefined') {
    texture.format = opt.format
  } else {
    // choose a nice default format
    const isJPEG = url.search(/\.(jpg|jpeg)$/) > 0 || url.search(/^data\:image\/jpeg/) === 0;
    texture.format = isJPEG ? RGBFormat : RGBAFormat
  }
  if (opt.repeat) texture.repeat.copy(opt.repeat)
  texture.wrapS = opt.wrapS || ClampToEdgeWrapping
  texture.wrapT = opt.wrapT || ClampToEdgeWrapping
  texture.minFilter = opt.minFilter || LinearMipMapLinearFilter
  texture.magFilter = opt.magFilter || LinearFilter
  texture.generateMipmaps = opt.generateMipmaps !== false
}


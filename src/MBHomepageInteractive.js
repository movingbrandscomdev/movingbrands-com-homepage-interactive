import EventEmitter from 'eventemitter3'
import ScrollMonitor from 'scrollmonitor'
import shuffle from 'array-shuffle'

import {
  fetchSupport,
  log
} from './utils'
import { html } from './dom'
import { initCSS } from './dom/css'
import {
  PresetManager,
  AssetManager
} from './presets'
import { WebGLApp } from './webgl'
import { Item } from './webgl/items'
import { RenderViewManager } from './webgl/views'
import { Controller } from './ui'
import { VERSION, MODULE_NAME } from './constants'

/**
 * Application base
 * @class MBHomepageInteractive
 */

class MBHomepageInteractive extends EventEmitter {
  static get READY() { return 'ready' }
  static get VISIBLE() { return 'visible' }
  static get HIDDEN() { return 'hidden' }

  /**
   * Initiates the app
   * @param {HTMLElement} targetDOMElement - a valid HTML div that can act as a container for the WebGLApp canvas.
   * @param {object} presetData - object with settings
   * @param {object} config - configuration object
   * @param {function} response - callback
   */
  constructor(targetDOMElement, presetData, config, response) {
    super()

    if (!targetDOMElement instanceof HTMLDivElement) {
      throw new TypeError('targetDOMElement must be a valid <div>')
    }

    const { views, presets } = presetData

    if (!presets instanceof Array && !presets.length > 0) {
      throw new TypeError('presets must be a valid array')
    }
    if (!views instanceof Array && !views.length > 0) {
      throw new TypeError('views must be a valid array')
    }

    this.config = Object.assign({}, config)
    this.el = targetDOMElement
    Controller.init(this.el)
    this.el.style.overflowX = 'hidden'
    document.body.style.overflowX = 'hidden'
    this.support = fetchSupport()

    this.state = {
      ready: false
    }
    initCSS()
    log(`${MODULE_NAME} v${VERSION} / ⌘+u to debug`)

    this.app = WebGLApp.init(this.el, this.config, views)
    this.el.appendChild(html('div', 'overlay', null, { aria: { hidden: 'true' } }))
    
    this.loadPresets(presets, response)

  }

  /**
   * Adds data for a new {@link RenderView} to the {@link RenderViewManager}
   * @param {object} view - valid data for new {@link RenderView}
   */
  addView(view) {
    if (view) {
      RenderViewManager.addView(view, this.app.params.width, this.app.params.height)
    }
  }

  /**
   * Adds an array of {@link Preset}s to {@link PresetManager}, loading files via {@link AssetManager}
   * @param {array} newPresets - array of presets to add
   * @param {function} response - (optional) callback
   */
  loadPresets(newPresets, response) {
    PresetManager.load(this.config.shuffle ? shuffle(newPresets) : newPresets)
    PresetManager.on('preload-complete', loadedPresets => {
      loadedPresets.forEach(preset => this.app.addItem(new Item(preset)))
      this.onFirstReady()
    })
    if (response) response(this.support)
  }
  onFirstReady() {
    this.emit(MBHomepageInteractive.READY)
    this.app.emit(WebGLApp.SET_PRESET)
    this.state.ready = true
    this.monitor()

    if (!(this.config.ui && !this.config.ui.buttons)) {
      Controller.addItems(this, WebGLApp)
    }
    this.app.path.moveTo(0)
    this.app.emit('app-ready')

  }

  /**
   * (internal) Sets up scroll monitor to watch container. Automatically triggers start() and stop() when container is scrolled in and out of view
   */
  monitor() {
    this.monitor = ScrollMonitor.create(this.el)
    this.monitor.enterViewport(this.start.bind(this))
    this.monitor.exitViewport(this.stop.bind(this))
  }

  /**
   * Starts animation loop
   */
  start() {
    this.emit(MBHomepageInteractive.VISIBLE)
    if (this.app && this.state.ready) this.app.start()
  }

  /**
   * Stops animation loop
   */
  stop() {
    this.emit(MBHomepageInteractive.HIDDEN)
    if (this.app) this.app.stop()
  }
}

export default MBHomepageInteractive
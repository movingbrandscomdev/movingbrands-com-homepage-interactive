import { AssetManager } from './.'
import Promise from 'promise-polyfill'

/**
 * Loads asset via @AssetManager
 * @function assetLoadPromise
 * @param {String} assetType – asset type (GLTF, image...)
 * @param {String} path – file url
 * @returns {Promise<Object>} – returns {@link Asset} reference object
 */
const assetLoadPromise = (assetType, path) => {
  return new Promise((resolve, reject) => AssetManager.addAsset(assetType, path, resolve, reject))
}

export default assetLoadPromise

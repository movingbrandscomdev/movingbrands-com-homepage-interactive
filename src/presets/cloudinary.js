import { CLOUDINARY } from './constants'

const fetchCloudinaryURL = (assetType, asset) => {

  const { FOLDER, BASE_URL } = CLOUDINARY
  const { id } = asset
  const raw = assetType === 'gltf'
  const matcap = assetType === 'matcap'
  const texture = assetType === 'texture'
  const alphaMap = assetType === 'alphaMap'
  if (id.startsWith('http://') || id.startsWith('https://')) {
    return id
  } else {
    return `${BASE_URL}/${raw ? 'raw' : 'image'}/upload/${cloudinaryTransformation(assetType, asset)}${id}${texture || matcap || alphaMap ? '.jpg' : ''}`
  }
}

const cloudinaryTransformation = (assetType, asset) => {
  const { JPG_QUALITY, CHROMA, CHROMA_BASE } = CLOUDINARY
  let transformation = asset.effects && asset.effects.blur ? `e_blur:${asset.effects.blur},` : ''

  if (assetType === 'alphaMap') {
    transformation += `e_colorize,co_${CHROMA},b_${CHROMA_BASE},q_55,${sizePowerOfTwo(asset.size)},f_auto/`
  } else if (assetType === 'texture' || assetType === 'matcap') {
    transformation += `q_${JPG_QUALITY},${sizePowerOfTwo(asset.size)},f_auto/`
  }
  return transformation
}

const sizePowerOfTwo = size => {
  return `w_${nearestPowerOfTwo(size.width)},h_${nearestPowerOfTwo(size.height)}`
}

const nearestPowerOfTwo = num => {
  return Math.pow(2, Math.round(Math.log(num) / Math.log(2)))
}

export {
  fetchCloudinaryURL
}
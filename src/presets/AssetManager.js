import EventEmitter from 'eventemitter3'

import { Asset } from './.'
import {
  GLTFLoader,
  TextureLoader
} from '../webgl/loaders'

import { fetchCloudinaryURL } from './cloudinary'
/**
 * Handles the loading and management of {@link Asset}s
 * (singleton)
 */
class AssetManager extends EventEmitter {
  constructor() {
    super()
    this.assets = []
    this.loaders = {
      image: new TextureLoader(),
      gltf: new GLTFLoader()
    }
  }
  static get ASSETS_UPDATED() {
    return 'assets-updated'
  }
  /**
     * Creates a new {@link Asset} and adds to {@link AssetManager}
		 * @param {object} newAsset - asset meta data
		 * @param {object} data - asset as three.js texture/mesh
     * @param {function} respond - callback with new {@link Asset}
     */
  add(newAsset, data, respond) {
    const a = new Asset(newAsset, data)
    this.assets.push(a)
    if (respond) respond(a)
    this.emit(AssetManager.ASSETS_UPDATED, this.assets)
  }

  /**
     * Fetches a single {@link Asset}
     * @param {string} asset - asset reference
     * @param {function} respond - (optional) callback
     * @returns {Asset} – asset
     */
  fetch(asset, respond) {
    if (!asset) throw new TypeError('Please provide a valid ID or object')
    let f

    if (asset instanceof String) {
      f = this.assets.find(a => a.id === asset)
    } else if (asset instanceof Object) {
      f = {}
      Object.keys(asset).forEach(k => {
        f[k] = this.assets.find(a => a.id === asset[k].id)
      })
    }
    if (!f) throw new Error(`Could not find an Asset with ID: ${assetId}`)
    if (respond) respond(f)
    return f
  }
  /**
     * Checks incoming new {@link Asset} to see if already exists
     * @param {string} assetType - type of file (e.g. Image, OBJ, GLTF)
     * @param {object} asset - asset url or reference reference
     * @param {function} resolve - callback if Asset creation succeeds
     * @param {function} reject - callback if Asset creation fails
     */
  addAsset(assetType, asset, resolve, reject) {
    const loaderType = assetType === 'gltf' ? 'gltf' : 'image'
    // if the Asset doesn't exist in the AssetManager, preload and add a new Asset
    const assetToLoad = Object.assign(asset, {
      path: fetchCloudinaryURL(assetType, asset),
      type: assetType
    })
    this.loaders[loaderType].load(assetToLoad.path, data => {
      if (data) {
        this.add(assetToLoad, data, a => { resolve(a.ref) })
      }
      else {
        reject(false, 'Download failed')
      }
    }, prog => {

    }, err => {
      console.log(err)
    })
  }
}


export default new AssetManager()

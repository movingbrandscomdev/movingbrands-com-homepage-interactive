import EventEmitter from 'eventemitter3'
import Promise from 'promise-polyfill'
import { Preset } from './.'
import { log } from '../utils'

/**
 * Loads and manages Presets in the application
 * (singleton)
 */
class PresetManager extends EventEmitter {
  static get TIMED_STATUS () { return 'Presets and assets loaded' }
  static get PRELOAD_COMPLETE () { return 'preload-complete' }
  static get ASSETS () { return this.loadedAssets }

  constructor () {
    super()
    this.loadedPresets = []
  }
  load (newPresets) {
    console.time(PresetManager.TIMED_STATUS)
    Promise.all(newPresets.map(preset => this.loadPreset(preset))).then((res) => {
      this.emit(PresetManager.PRELOAD_COMPLETE, res)
      console.timeEnd(PresetManager.TIMED_STATUS)
    }).catch(err => console.warn(err))
  }
  loadPreset (preset) {
    return new Promise((resolve, reject) => {
      const newPreset = new Preset(preset)
      newPreset.loadLayers(preset.layers).then((res, err) => {
        if (res) {
          this.loadedPresets.push(newPreset)
          resolve(newPreset)
        } else {
          reject('Asset preload failed')
        }
      })
      return true
    })
  }
  fetchPreset (num) {
    return this.loadedPresets[num] ? this.loadedPresets[num] : null
  }
  fetchViews (num) {
    return this.loadedPresets[num] ? this.loadedPresets[num].views : {}
  }
  fetch (response) {
    response(this.loadedPresets)
  }
}

export default new PresetManager()

export const ASSET_CATEGORIES = [
    'matcap',
    'texture',
    'alphaMap',
    'gltf'
]

export const CLOUDINARY = {
    BASE_URL: 'https://res.cloudinary.com/moving-brands',
    FOLDER: 'movingbrands.com',
    JPG_QUALITY: 60,
    CHROMA: 'black',
    CHROMA_BASE: 'white',
  }

import Promise from 'promise-polyfill'
import {
  randomId,
  assignIds
} from '../utils'
import { AssetManager } from './.'
import { ASSET_CATEGORIES } from './constants'
/**
 * Represents each case study in the homepage interactive. Made from data, plugins and Assets
 * @class Preset
 */
export default class Preset {
  static get PRESET_ID() { return randomId(8, 'preset_') }
  static get LAYER_ID() { return randomId(8, 'layer_') }
  /**
         * Creates a new Preset
         * @param {object} preset - preset data from JSON file
         * @returns {Preset} – returns {@link Preset}
        */
  constructor(preset) {
    this.id = Preset.PRESET_ID

    this.data = Object.assign({}, {
      name: preset.name,
      description: preset.description
    })
    if (preset.link) this.data.link = preset.link
    if (preset.headline) this.data.headline = preset.headline
    this.plugins = assignIds(preset.plugins, 'motionplugin') || []
    this.views = preset.views || {}
    return this
  }

  loadLayers(layers) {
    return new Promise.all(layers.map(layer => {
      const result = {
        type: layer.type,
        id: Preset.LAYER_ID,
        config: Object.assign({}, layer.config)
      }
      const fetchLayerAssets = ASSET_CATEGORIES.filter(ac => {
        return layer[ac] ? ac : false
      }).map(ac => {
        return new Promise((resolve, reject) => AssetManager.addAsset(ac, layer[ac], resolve, reject))
      })

      return new Promise.all(fetchLayerAssets).then(res => {
        if (res) {
          return parsePresetAssets(result, res)
        }
      }).catch(err => {
        throw new Error(`Couldn't fetch all ${fetchLayerAssets.length} requested assets`)
      })
    })).then((res, err) => {
      this.layers = res
      return res
    })
  }
}

const parsePresetAssets = (o, resArray) => {
  const result = Object.assign({ assets: {} }, o)

  resArray.forEach(assetReference => result.assets[assetReference.type] = assetReference)

  if (result.config.autoRatio && result.assets.texture) {
    const { width, height } = result.assets.texture.size
    result.config.scale = {
      x: result.config.scale,
      y: result.config.scale * (height / width),
      z: result.config.scale
    }
  }
  return result
}

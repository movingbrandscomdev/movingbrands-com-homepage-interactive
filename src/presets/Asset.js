import { randomId } from '../utils'

/**
 * Holds a single file (Image, OBJ, GLTF)
 * @class Asset
 */
class Asset {
	static get ASSET_ID() { return randomId(18, 'asset_') }
	/**
		 * Creates an Asset.
		 * @param {object} assetData - asset meta data
		 * @param {object} data - asset as three.js texture/mesh
		 */
	constructor(assetData, data) {
		this.id = Asset.ASSET_ID
		this.type = assetData.type
		this.path = assetData.path
		this.data = data
		if (assetData.size) {
			this.size = {
				width: assetData.size.width,
				height: assetData.size.height
			}
		}
	}
	get ref() {
		const a = {
			id: this.id,
			path: this.path,
			type: this.type
		}
		if (this.size) a.size = this.size
		return a
	}
}

export default Asset

import { mapRange, constrain } from '../utils'

/**
 * @function handleMouse
 * Desktop mouse motion handler
 */
const handleMouse = (e, result) => {
  const move = {
    x: -1.0 + e.pageX / window.innerWidth * 2,
    y: -1.0 + e.pageY / window.innerHeight * 2
  }
  result({
    x: constrain(move.y / 4, -0.3, 0.3),
    y: constrain(move.x / 4, -0.3, 0.3)
  })
}

/**
 * @function handleOrientation
 * Mobile device orientation handler
 */
const handleOrientation = (e, result) => {
  const y = mapRange(e.gamma, -60, 60, -0.25, 0.25)
  const x = mapRange(e.beta, -60, 60, -0.25, 0.25)
  result({
    x: x,
    y: y
  })
}

export {
  handleMouse,
  handleOrientation
}

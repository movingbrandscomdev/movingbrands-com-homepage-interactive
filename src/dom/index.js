import html from './html'

const addClass = (el, className) => {
  if (el instanceof Array) {
    el.forEach(c => c.classList.add(className))
  } else {
    el.classList.add(className)
  }
}
const removeClass = (el, className) => {
  if (el instanceof Array) {
    el.forEach(c => c.classList.remove(className))
  } else {
    el.classList.remove(className)
  }
}

export {
  html,
  addClass,
  removeClass
}

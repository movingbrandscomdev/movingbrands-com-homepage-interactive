import { elementProp } from './css'

/**
 * Very basic helper to create simple div elements
 * @function html
 * @param {string} elementType - HTML element type (div, a etc.)
 * @param {string} className - class name
 * @param {string} content - class name
 * @param {object} attributes - optional additional (href, onclick)
 * @returns {HTMLElement} element
*/
const html = (elementType, className, content, attributes) => {
  const el = document.createElement(elementType)
  el.className = `${elementProp}${className}`
  if (content) append(content, el)
  if (attributes) {
    if (attributes.onclick && typeof attributes.onclick === 'function') {
      el.addEventListener('click', (e) => { attributes.onclick(e) })
    }
    if (attributes.onkeyup && typeof attributes.onkeyup === 'function') {
      el.addEventListener('keyup', (e) => { attributes.onkeyup(e) })
    }
    if (attributes.hasOwnProperty('tabindex')) {
      el.tabIndex = attributes.tabindex
    }
    if (attributes.aria) {
      Object.keys(attributes.aria).forEach(attr => {
        if (attr === 'role') {
          el.setAttribute(attr, attributes.aria[attr])
        } else {
          el.setAttribute(`aria-${attr}`, attributes.aria[attr])
        }
      })
    }
    if (elementType === 'a' && attributes.href) {
      el.href = attributes.href
    }
  }
  return el
}

const append = (content, target) => {
  if (content instanceof Array) {
    content.forEach(c => appendHtml(c, target))
  } else {
    appendHtml(content, target)
  }
}

const appendHtml = (content, target) => {
  if (content instanceof Element) {
    target.appendChild(content)
  } else {
    target.innerHTML = content
  }
}

export default html

import { randomId } from '../utils'

const namespace = randomId(4, 'mbhi_')
const cssprop = `.${namespace}.${namespace}__`
const elementProp = `${namespace} ${namespace}__`

const css = `
    @keyframes ${namespace}fadeIn {
        0% {
            opacity: 0.0;
        }
        100% {
            opacity: 1.0;
        }
    } 
    @-webkit-keyframes ${namespace}fadeIn {
        0% {
            opacity: 0.0;
        }
        100% {
            opacity: 1.0;
        }
    } 
    @keyframes ${namespace}spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    } 
    @-webkit-keyframes ${namespace}spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    } 
    .${namespace},
    .${namespace}:after,
    .${namespace}:before {
        box-sizing: border-box;
        -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
        -webkit-tap-highlight-color: transparent;
    }
    .dg.ac {
        z-index: 500;
    }
    canvas.${namespace} {
        position: absolute;
        top: 0px;
        left: 0px;
        overflow: hidden;
    }
    ${cssprop}arrow {
        position: absolute;
        bottom: 0;
        z-index: 2;
        width: 60px;
        height: calc(100vh - 70px);
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
        background: white;
        opacity: 0.0;
        outline: 0;
    }
    ${cssprop}arrow.left {
        left: 0;
    }
    ${cssprop}arrow.right {
        right: 0;
    }

    ${cssprop}arrow:focus {
        opacity: 0.2;
    }

    ${cssprop}nav-container {
        position: absolute;
        bottom: 50px;
        width: 100vw;
        height: auto;
        left: 0;
        z-index: 5;
        display: flex;
        align-items: center;
        justify-content: flex-start;
        flex-direction: column;   
        opacity: 0.0;
        animation: ${namespace}fadeIn 0.3s forwards ease-in-out; 
        -webkit-animation: ${namespace}fadeIn 0.3s forwards ease-in-out; 
    }
    
    ${cssprop}nav-container::after {
        position: absolute;
        bottom: 0;
        width: 30px;
        height: 30px;
        content: '';
        border-radius: 50%;
        border-top: initial;
        border-left: initial;
        opacity: 1.0;
        transition: opacity 0.4s ease-in-out;
        -webkit-transition: opacity 0.4s ease-in-out;
        animation: ${namespace}spin 1.0s infinite ease-in-out; 
        -webkit-animation: ${namespace}spin 1.0s infinite ease-in-out; 
    }
    ${cssprop}nav-container.content-loaded::after {
        opacity: 0.0;
    }
    
    ${cssprop}nav-dot-tray {
        width: auto;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 15;

    }
    ${cssprop}nav-item {
        position: absolute;
        text-align: center;
        bottom: 0;
        opacity: 0.0;
        transform: translateX(4px);
        -webkit-transform: translateX(4px);
        transition: all 0.4s ease-in-out;
        -webkit-transition: all 0.4s ease-in-out;
        pointer-events: none;
        cursor: pointer;
    }
    
    ${cssprop}nav-caption-tray { 
        position:relative;
        width: 100%;
        height: 24px;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 15;
        margin-bottom: 10px;
    }

    ${cssprop}nav-item-headline {
        opacity: 0.6;
    }
    ${cssprop}nav-item.live.active {
        opacity: 1.0;
        pointer-events: initial;
        transform: translateX(0px);
        -webkit-transform: translateX(0px);
    }
    ${cssprop}nav-item-link {
        color: white;
        cursor: pointer;
        text-decoration: none;
    }
    ${cssprop}arrow.right:hover::after {
        // transform: translate3d(9px, 0, 0);
    }
    ${cssprop}arrow.left:hover::after {
        // transform: translate3d(-9px, 0, 0);
    }
    ${cssprop}d-pad {
        top: 0;
        left: 0;
        outline: 1px solid rgba(0,0,0,0);
        position: absolute;
        z-index: 5;
        border-radius: 2px;
        cursor: move;
        display: none;
        pointer-events: none;
        outline-color: rgb(160,160,160);
        box-shadow: 1px 1px 10px 0 rgba(0,0,0,0.3);
    }
    ${cssprop}d-pad:hover {
        outline-color: white;
    }
    ${cssprop}d-pad::after {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1000;
        content: 'Master scene view';
        color: white;
        padding: 0.5em;
        font-size: 12px;
    }
    ${cssprop}d-pad.show {
        display: initial;
        pointer-events: initial;
    }
    ${cssprop}overlay {
        width: 100%;
        height: 50vh;
        pointer-events: none;
        z-index: 0;
        position: absolute;
        bottom: -2px;
        left: 0;
        background: linear-gradient(to top, rgba(0, 0, 0, 1.0) 0%, rgba(0, 0, 0, 0.738) 19%, rgba(0, 0, 0, 0.541) 34%, rgba(0, 0, 0, 0.382) 47%, rgba(0, 0, 0, 0.278) 56.5%, rgba(0, 0, 0, 0.194) 65%, rgba(0, 0, 0, 0.126) 73%, rgba(0, 0, 0, 0.075) 80.2%, rgba(0, 0, 0, 0.042) 86.1%, rgba(0, 0, 0, 0.021) 91%, rgba(0, 0, 0, 0.008) 95.2%, rgba(0, 0, 0, 0.002) 98.2%, rgba(0, 0, 0, 0) 100%);
    }
    ${cssprop}nav-dot {
        width: 32px;
        height: 32px;
        cursor: pointer;
        position: relative;
        outline: 0;
    }
    ${cssprop}nav-dot::after {
        content: '';
        position: absolute;
        width: 16px;
        height: 16px;
        top: calc(50% - 8px);
        left: calc(50% - 8px);
        border-radius: 8px;
        background: rgba(255,255,255,0.4);
        background: white;
        pointer-events: none;
        opacity: 0.0;
        transform: scale(0.0);
        -webkit-transform: scale(0.0);
        transition: all 0.4s ease-in-out;
        -webkit-transition: all 0.4s ease-in-out;
    }

    ${cssprop}nav-dot.live::after {
        opacity: 0.4;
        transform: scale(0.5);
        -webkit-transform: scale(0.5);
    }
    ${cssprop}nav-dot:hover,
    ${cssprop}nav-dot::after:hover {
        cursor: pointer;
    }
    ${cssprop}nav-dot:not(.active):focus::after,
    ${cssprop}nav-dot:not(.active):hover::after {
        transform: scale(0.75);
        -webkit-transform: scale(0.75);
        opacity: 0.75;
    }
    ${cssprop}nav-dot.active::after {
        opacity: 1.0;
        transform: scale(1.01);
        -webkit-transform: scale(1.01);
    }
    @media (max-width: 500px) {
        ${cssprop}nav-dot {
            width: 12px;
            height: 20px;
            margin: 0;
            pointer-events: none;
        }
        ${cssprop}nav-dot::after {
            width: 12px;
            height: 12px;
            top: calc(50% - 6px);
            left: calc(50% - 6px);
        }
        ${cssprop}nav-dot.active::after {
            opacity: 1.0;
            transform: scale(0.5);
            -webkit-transform: scale(0.5);
        }
        ${cssprop}arrow {
            pointer-events: none;
            display: none;
        }
    }

`

const minify = (_css) => {
    // remove line breaks, spaces and returns in css string
    return _css.trim()
        .replace(/\/\*[\s\S]+?\*\//g, '')
        .replace(/[\n\r]/g, '')
        .replace(/\s*([:;,{}])\s*/g, '$1')
        .replace(/\s+/g, ' ')
        .replace(/;}/g, ';}')
}

const initCSS = () => {
    // create css element from css above and adds to document head
    const element = document.createElement('style')
    element.setAttribute('type', 'text/css')
    if ('textContent' in element) {
        element.textContent = minify(css)
    } else {
        element.styleSheet.cssText = minify(css)
    }
    document.getElementsByTagName('head')[0].appendChild(element)
}

export {
    namespace,
    elementProp,
    initCSS
}


/**
 * Helper class to store and calculate momentum along a single axis
 * @class Velocity
 */
export default class Velocity {
  constructor () {
    this.queue = []
    this.timeQueue = []
  }

  reset () {
    this.queue.splice(0)
    this.timeQueue.splice(0)
  }
  updateQueue (t) {
    while (this.timeQueue.length && this.timeQueue[0] < Date.now() - t) {
      this.timeQueue.shift()
      this.queue.shift()
    }
  }
  updatePosition (position) {
    this.queue.push(position)
    this.timeQueue.push(Date.now())
    this.updateQueue(50)
  }
  getVelocity () {
    this.updateQueue(1000)
    const length = this.timeQueue.length
    if (length < 2) return 0

    const distance = this.queue[length - 1] - this.queue[0]
    const time = this.timeQueue[length - 1] - this.timeQueue[0]

    return distance / time
  }
}

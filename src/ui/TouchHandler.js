import EventEmitter from 'eventemitter3'

import Touches from './Touches'
import TouchPhysics from './TouchPhysics'
import { isInsideElement, findClosest } from '../utils'

const touchEvents = [
  'start',
  'move',
  'end'
]

export default class TouchHandler extends EventEmitter {

  static get CHANGE() {
    return 'change'
  }
  static get TOUCH_START() {
    return 'start'
  }
  static get TOUCH_MOVE() {
    return 'move'
  }
  static get TOUCH_END() {
    return 'end'
  }
  static get TOUCH_EVENTS() {
    return [
      TouchHandler.TOUCH_START,
      TouchHandler.TOUCH_MOVE,
      TouchHandler.TOUCH_END
    ]
  }

  constructor(node, itemCount) {
    super()
    this.domNode = node || document.body
    this.init(itemCount)
  }

  init(itemCount, active) {
    this.params = {
      width: window.innerWidth,
      height: this.domNode.offsetHeight || window.innerHeight
    }
    this.activePanel = !!active || 0

    this.touchPhysics = new TouchPhysics({
      totalCells: itemCount,
      viewSize: this.params.width,
      cellSize: this.params.width,
      gutterSize: this.params.width / itemCount,
      dipMaxSpeed: 20,
      snappiness: 0.4,
      dipToClosestCell: true,
      velocity: false
    })
    this.listen()
  }

  update(dt) {
    const f = Math.round(this.x)
    if (f !== this.activePanel) {
      this.activePanel = f
      this.emit(TouchHandler.CHANGE)
    }
    this.touchPhysics.update(dt)
  }
  resize(count) {
    this.params.width = window.innerWidth
    this.params.height = this.domNode.offsetHeight || window.innerHeight

    this.touchPhysics.resize(this.params.width, count)
  }
  listen() {
    const touchListener = Touches(this.domNode, {
      target: this.domNode,
      filtered: true
    })

    TouchHandler.TOUCH_EVENTS.forEach(name => {
      touchListener.on(name, (ev, pos) => {
        if (name === TouchHandler.TOUCH_START) {
          this.emit(TouchHandler.TOUCH_START)
          if (!isInsideElement(pos, this.domNode)) {
            return
          }
        }
        if (name === TouchHandler.TOUCH_END) {
          this.emit(TouchHandler.TOUCH_END)
        }
        this.touchPhysics[name](pos[0])
      })
    })
  }
  get x() {
    return this.touchPhysics.value / this.params.width
  }
}


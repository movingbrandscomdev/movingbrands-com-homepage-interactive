import EventEmitter from 'eventemitter3'
import { DOM_REFERENCE_NODE } from '../constants'
import { html, addClass, removeClass } from '../dom'

/**
 * Controller for carousel
 * @class Controller
 */
class Controller extends EventEmitter {
  constructor(parent) {
    super()
    this.params = {
      debug: false
    }

    this.state = {
      active: 0
    }

    this.items = []
  }
  init(target) {
    this.domParent = target
    this.navContainer = html('div', 'nav-container', null)
    this.domParent.appendChild(this.navContainer)
  }
  addItems(parent, webglapp) {
    const { path, items } = webglapp

    this.state.debug = parent.config.debug || false
    const arrows = [
      html('button', 'arrow left', null, {
        onkeyup: e => {
          if (e.key === ' ') {
            e.preventDefault()
            e.stopPropagation()
            path.moveTo(-1)
          } else if (e.key === 'Enter') {
            path.moveTo(-1)
          }
        }, onclick: e => path.moveTo(-1), aria: { label: 'prev' }, tabindex: 0
      }),
      html('button', 'arrow right', null, {
        onkeyup: e => {
          if (e.key === ' ') {
            e.preventDefault()
            e.stopPropagation()
            path.moveTo(1)
          } else if (e.key === 'Enter') {
            path.moveTo(1)
          }
        }, onclick: e => path.moveTo(1), aria: { label: 'next' }, tabindex: 0
      })
    ]

    this.captionTray = html('div', 'nav-caption-tray', null, null)
    this.navContainer.appendChild(this.captionTray)

    arrows.forEach(el => this.navContainer.appendChild(el))

    this.navTray = html('div', 'nav-dot-tray', arrows, { aria: { label: 'Image carousel', controls: DOM_REFERENCE_NODE } })
    this.navContainer.appendChild(this.navTray)


    webglapp.items.forEach((p, n) => {
      this.items.push(new NavigationElement({ dotsTray: this.navTray, captionTray: this.captionTray }, p.config, n, path.set))
    })

    this.update()
    addClass(this.navContainer, 'content-loaded')
  }
  update(n) {
    this.items.forEach((p, ref) => {
      p.toggle(ref, n)
    })
  }
}

class NavigationElement {
  constructor(...args) {
    this.init(...args)
  }
  init(targets, preset, num, onclick) {
    this.dot = html('div', 'nav-dot', null, {
      onkeyup: e => {
        if (e.key === ' ') {
          e.preventDefault()
          e.stopPropagation()
          onclick(num)
        } else if (e.key === 'Enter') {
          onclick(num)
        }
      },
      onclick: () => onclick(num), tabindex: 0, aria: { role: 'button', label: `Carousel item ${num + 1}` }
    })
    this.el = html('div', 'nav-item',
      [
        html('a', 'nav-item-link', preset.description, { href: preset.link })
      ]
    )

    targets.dotsTray.appendChild(this.dot)
    targets.captionTray.appendChild(this.el)

    setTimeout(() => addClass([this.el, this.dot], 'live'), 1 + num * (num * 15))
  }
  toggle(num, check) {
    if (check === num) {
      addClass([this.dot, this.el], 'active')
      this.el.setAttribute('aria-hidden', 'false')
    } else {
      removeClass([this.dot, this.el], 'active')
      this.el.setAttribute('aria-hidden', 'true')
    }
  }
}

export default new Controller()

import EventEmitter from 'eventemitter3'

const allEvents = [
  'touchstart', 'touchmove', 'touchend', 'touchcancel',
  'mousedown', 'mousemove', 'mouseup'
]

const ROOT = { left: 0, top: 0 }

/**
 * Derived from https://github.com/Jam3/touches/
 * @function Touches
 * @param {HTMLElement} element - DOM element to listen for interaction events
 * @param {object} opt - settings object
 * @returns emitter
 */

const Touches = (element, opt) => {
  opt = opt || {}
  element = element || window

  const emitter = new EventEmitter()
  emitter.target = opt.target || element

  let touch = null
  const filtered = opt.filtered

  let _events = allEvents

  // only a subset of _events
  if (typeof opt.type === 'string') {
    _events = allEvents.filter(function (type) {
      return type.indexOf(opt.type) === 0
    })
  }

  // grab the event functions
  const funcs = _events.map(function (type) {
    const name = normalize(type)
    const fn = function (ev) {
      let client = ev
      if (/^touch/.test(type)) {
        // if (/^touchend$/.test(type) && opt.preventSimulated !== false) {
        //   ev.preventDefault()
        // }

        if (filtered) {
          client = getFilteredTouch(ev, type)
        } else {
          client = getTargetTouch(ev.changedTouches, emitter.target)
        }
      }

      if (!client) {
        return
      }

      // get 2D position
      const pos = offset(client, emitter.target)

      // dispatch the normalized event to our emitter
      emitter.emit(name, ev, pos)
    }
    return { type: type, listener: fn }
  })

  emitter.enable = function enable() {
    funcs.forEach(listeners(element, true))

    return emitter
  }

  emitter.disable = function dispose() {
    touch = null
    funcs.forEach(listeners(element, false))

    return emitter
  }

  // initially enabled
  emitter.enable()
  return emitter

  function getFilteredTouch(ev, type) {
    let client

    // clear touch if it was lifted or canceled
    if (touch && /^touch(end|cancel)/.test(type)) {
      // allow end event to trigger on tracked touch
      client = getTouch(ev.changedTouches, touch.identifier || 0)
      if (client) {
        touch = null
      }
    } else if (!touch && /^touchstart/.test(type)) {
      // not yet tracking any touches, pick one from target
      touch = client = getTargetTouch(ev.changedTouches, emitter.target)
    } else if (touch) {
      // get the tracked touch
      client = getTouch(ev.changedTouches, touch.identifier || 0)
    }
    return client
  }
}



// get 2D client position of touch/mouse event
const offset = (ev, target) => {
  const cx = ev.clientX || 0
  const cy = ev.clientY || 0
  const rect = bounds(target)
  return [cx - rect.left, cy - rect.top]
}

// since we are adding _events to a parent we can't rely on targetTouches
const getTargetTouch = (touches, target) => {
  return Array.prototype.slice.call(touches).filter(function (t) {
    return t.target === target
  })[0] || touches[0]
}

const getTouch = (touches, id) => {
  for (let i = 0; i < touches.length; i++) {
    if (touches[i].identifier === id) {
      return touches[i]
    }
  }
  return null
}

const listeners = (e, enabled) => {
  return function (data) {
    if (enabled) e.addEventListener(data.type, data.listener, { passive: false })
    else e.removeEventListener(data.type, data.listener, { passive: false })
  }
}

// normalize touchstart/mousedown to "start" etc
const normalize = event => event.replace(/^(touch|mouse)/, '')
  .replace(/up$/, 'end')
  .replace(/down$/, 'start')


const bounds = element => {
  if (element === window ||
    element === document ||
    element === document.body) {
    return ROOT
  } else {
    return element.getBoundingClientRect()
  }
}

export default Touches
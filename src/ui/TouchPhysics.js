import defined from 'defined'

import { constrain } from '../utils'

/**
 * @module TouchPhysics
 * Arbitrary helper for managing touch/mouse interaction on canvas and snapping motion
 * Derived from https://github.com/Jam3/touch-scroll-physics/
 */

export default class TouchPhysics {
  constructor (opt) {
    opt = opt || {}

    this.value = 0
    this.momentum = 0

    this.totalCells = defined(opt.totalCells, 1)
    this.cellSize = defined(opt.cellSize, 1)
    this.viewSize = opt.viewSize || 1
    this.gutterSize = defined(opt.gutterSize, this.viewSize / 4)
    this.dipToClosestCell = opt.dipToClosestCell

    this.updateSize()

    this.lastInput = 0
    this.interacting = false

    this.dipMaxSpeed = opt.dipMaxSpeed || 10
    this.dipSnappiness = opt.snappiness || 0.2

    this.inputDelta = 0
    this.inputDeltaIndex = 0
    this.inputDeltaHistoryMax = 3
    this.inputDeltas = []
    for (let i = 0; i < this.inputDeltaHistoryMax; i++) {
      this.inputDeltas.push(0)
    }
  }
  update (dt) {
    const isBefore = this.value < 0
    const isAfter = this.value > this.max
    const isInside = !isBefore && !isAfter

    // ease input at edges
    if (isBefore) {
      this.momentum = 0
      if (this.inputDelta > 0) {
        this.inputDelta *= 1 - (this.value / -this.gutterSize)
      }
    } else if (isAfter) {
      this.momentum = 0
      if (this.inputDelta < 0) {
        this.inputDelta *= (this.maxGutter - this.value) / this.gutterSize
      }
    }

    const dipping = !this.interacting

    this.value -= this.cellSizeHalf

    let dip = 0

    if (dipping) {
      if (isInside && this.dipToClosestCell) {
        dip = (((this.value % this.cellSize) + this.cellSize) % this.cellSize) - this.cellSizeHalf
      } else if (isBefore) {
        dip = this.value + this.cellSizeHalf
      } else if (isAfter) {
        dip = this.value - this.max + this.cellSizeHalf
      }
      const dipStrength = (1 - constrain(Math.abs(this.momentum) / this.dipMaxSpeed, 0, 1)) * this.dipSnappiness
      dip *= dipStrength
    }
    this.value -= this.inputDelta
    this.inputDelta = 0
    this.value -= this.momentum
    this.momentum *= 0.9
    this.value -= dip

    this.value += this.cellSizeHalf
    this.value = constrain(this.value, -this.gutterSize, this.maxGutter)
  }

  start (value) {
    this.interacting = true
    this.momentum = 0
    this.inputDelta = 0
    this.lastInput = value
  }

  move (value) {
    if (this.interacting) {
      this.inputDelta = value - this.lastInput
      this.inputDeltas[this.inputDeltaIndex] = this.inputDelta
      this.inputDeltaIndex = (this.inputDeltaIndex + 1) % this.inputDeltaHistoryMax
      this.lastInput = value
    }
  }
  updateSize () {
    this.cellSizeHalf = this.cellSize * 0.5
    this.fullSize = Math.max(this.viewSize, this.cellSize * this.totalCells)
    this.max = this.fullSize - this.viewSize
    this.maxGutter = this.max + this.gutterSize
  }
  resize (w, count) {
    this.viewSize = w
    this.cellSize = w
    this.gutterSize = w / count
    this.updateSize()
  }
  end (value) {
    if (this.interacting) {
      this.interacting = false
      this.momentum = this.inputDeltas.reduce(function (a, b) {
        return Math.abs(a) > Math.abs(b) ? a : b
      }, 0)
      this.delta = 0
      for (let i = 0; i < this.inputDeltaHistoryMax; i++) {
        this.inputDeltas[i] = 0
      }
    }
  }
}

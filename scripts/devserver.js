var browserSync = require('browser-sync')

/**
 * Run Browsersync with server config
 */
browserSync({
    ghostMode: true,
    server: 'examples',
    files: [
        'examples/*.html',
        'examples/mbHomepageInteractive.js',
        'examples/presetData.json',
        'examples/presetData_local.json'
    ]
})

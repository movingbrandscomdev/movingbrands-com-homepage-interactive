const fs = require('fs')

fs.readFile(someFile, 'utf8', (err, data) => {
    if (err) {
        return console.log(err)
    }
    const result = data.replace(/string to be replaced/g, 'replacement')

    fs.writeFile(someFile, result, 'utf8', (err) => {
        if (err) return console.log(err)
    })
})

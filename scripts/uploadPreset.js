const argv = require('yargs').argv
const fs = require('fs')
const path = require('path')
const cloudinary = require('cloudinary').v2

const CONSTANTS_PATH = 'src/presetData_constant.js'

cloudinary.config({
    cloud_name: 'moving-brands',
    api_key: '586657259582178',
    api_secret: '9IgLQVzXX2f1E0sGJsgpbYnEdP0'
})

const randomId = (len, append) => [append || '', Math.random().toString(36).replace(/[^a-z]+/g, '')].join('')

const options = {
    path: argv.path || argv.p || false
}

if (options.path && fs.existsSync(options.path)) {
    cloudinary.uploader.upload(options.path, { tags: 'MOVINGBRANDS.COM_PRESET', resource_type: 'raw', folder: 'movingbrands.com/PRESET' }, (err, res) => {
        if (err) {
            console.log('Upload failed')
        } else {
            console.log('JSON uploaded')
            fs.writeFile(CONSTANTS_PATH, `export const PRESETDATA_URL = '${res.secure_url}'`, err => {
                if (err) {
                    return console.log(err)
                }
                console.log(`URL ref updated at ${CONSTANTS_PATH}`)
            })
        }
    })
} else {
    console.log('Please supply a valid file path')
}

